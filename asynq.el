;;;_ tinydb/asynq.el --- Asynchrous queue for use with Elisp

;;;_. Headers
;;;_ , License
;; Copyright (C) 2010  Tom Breton (Tehom)

;; Author: Tom Breton (Tehom) <tehom@panix.com>
;; Keywords: lisp, internal

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;;_ , Commentary:

;; This complements tq transaction queues.  Tq is meant for use with
;; processes, and doesn't play nicely with Elisp code.


;;;_ , Requires
(eval-when-compile 
   (require 'cl))

;;;_. Body

;;;_ , Transaction management
;;;_  . Struct tinydb-q
(defstruct (tinydb-q
	      (:constructor tinydb-make-q
		 (setup get put type-pred
		    &rest args
		    &aux
		    (queue '())
		    (obj (apply setup args))))
	      (:conc-name tinydb-q->)
	      (:copier nil))
   "A transaction queue that works with native elisp rather than an
inferior process."
   queue
   obj
   get
   put
   type-pred)

;;;_  . tinydb-q-check-type
(defun tinydb-q-check-type (tq obj)
   ""
   (let
      ((type-pred (tinydb-q->type-pred tq)))
      (if
	 (or (null type-pred) (funcall type-pred obj))
	 obj
	 (error
	    "In tinydb-q, object %s was wrong type" obj))))

;;;_  . tinydb-check-listlock
(defmacro tinydb-check-listlock (sym obj &rest err-args)
   ""
   (declare (debug (symbolp form &rest sexp)))
   `(when (and
	     (boundp ',sym)
	     (memq ,obj ,sym))
       (error ,@err-args)))
;;;_  . tinydb-with-listlock
(defmacro tinydb-with-listlock (sym obj &rest body)
   ""
   (declare (debug (symbolp form body)))
   `(let
       ((,sym 
	   (if (boundp ',sym)
	      (cons ,obj ,sym)
	      (list ,obj))))
       ,@body))

;;;_  . tinydb-q-do-pending
(defun tinydb-q-do-pending (tq)
   "Do all pending operations.
If another call to the same tq is active, raise an error."
   (declare (special persist-*handler-running*))
   '  ;;$$FIX ME  Quoted out because async operations are buggy
   (tinydb-check-listlock 
      persist-*handler-running* tq
      "`tinydb-q-do-pending' called while already running.")
   '  ;;$$FIX ME  Quoted out because async operations are buggy
   (while (tinydb-q->queue tq)
      (tinydb-with-listlock persist-*handler-running* tq
	 (let*
	    (
	       (obj (funcall (tinydb-q->get tq) (tinydb-q->obj tq)))
	       (cell (pop (tinydb-q->queue tq))))
	    (condition-case nil
	       (catch 'tinydb-q-no-change
		  (let
		     ((new-obj
			 (catch 'tinydb-q-new-obj
			    (throw 'tinydb-q-no-change
			       (apply (car cell) obj (cdr cell))))))
		     ;;Check its type.  This can raise error.  If it
		     ;;does, control just goes to the next iteration
		     ;;without changing obj.
		     (tinydb-q-check-type tq new-obj)
		     ;;Replace the object.
		     (setf 
			(tinydb-q->obj tq)
			(funcall (tinydb-q->put tq)
			   (tinydb-q->obj tq) 
			   new-obj))))
		  
	       ;;On error, just go on to the next one.  The obj field
	       ;;has not been changed.
	       (error nil))))))



;;;_  . tinydb-q-will-call
(defun tinydb-q-will-call (tq now-p function &rest args)
   "Schedule FUNCTION to be called on TQ.
Function will:
 * Take TQ's internal object
 * Take ARGS as the rest of its args
 * Return value is ignored.
 * If FUNCTION should set a new value for TQ's internal object, throw
   that value to `tinydb-q-new-obj'"
   (check-type tq tinydb-q)
   (tinydb-check-listlock 
      persist-*handler-running* tq
      "`tinydb-q-do-pending' called while already running.")

   '  ;;$$FIX ME  Quoted out because async operations are buggy
   (progn
      (callf append (tinydb-q->queue tq) (list (list* function args)))
      (when now-p
	 (tinydb-q-do-pending tq)))
   (tinydb-with-listlock persist-*handler-running* tq
      (let*
	 (
	    (obj (funcall (tinydb-q->get tq) (tinydb-q->obj tq)))
	    (cell (pop (tinydb-q->queue tq))))
	 '  ;;$$FIX ME  Quoted out because async operations are buggy
	 (condition-case nil
	    t
		  
	    ;;On error, just go on to the next one.  The obj field
	    ;;has not been changed.
	    (error nil))

	 (catch 'tinydb-q-no-change
	    (let
	       ((new-obj
		   (catch 'tinydb-q-new-obj
		      (throw 'tinydb-q-no-change
			 (apply function obj args)))))
	       ;;Check its type.  This can raise error.  If it does,
	       ;;we exit without changing obj.
	       (tinydb-q-check-type tq new-obj)
	       ;;Replace the object.
	       (setf 
		  (tinydb-q->obj tq)
		  (funcall (tinydb-q->put tq)
		     (tinydb-q->obj tq) 
		     new-obj))))
	 ))
   )

;;;_ , Immediate getter
;;;_  . tinydb-get
(defun tinydb-get (tq func &rest args)
   ""
   (let ((holder (list nil)))
      (tinydb-q-will-call tq t
	 #'(lambda (obj holder func args)
	      (setcar holder (apply func obj args)))
	 holder
	 func
	 args)
      (car holder)))

;;;_ , Some specific handlers
;;;_  . Whole object - Exists mostly for testing
;;;_   , tinydb-set-obj
(defun tinydb-set-obj (tq x)
   ""
   (tinydb-q-will-call tq nil
      #'(lambda (obj x)
	   (throw 'tinydb-q-new-obj x))
      x))

;;;_   , tinydb-get-obj
(defun tinydb-get-obj (tq)
   ""
   (tinydb-get tq #'identity))

;;;_  . Alist
;;;_   , tinydb-alist-push
(defun tinydb-alist-push (tq key obj)
   "Push X onto an alist managed by TQ."
   (tinydb-q-will-call tq nil
      #'(lambda (alist key obj)
	   (throw 'tinydb-q-new-obj (cons (cons key obj) alist)))
      key obj))
;;;_   , tinydb-alist-pushnew
(defun tinydb-alist-pushnew (tq key obj)
   "Push X onto an alist managed by TQ, unless X's car is already a key on it."
   (tinydb-q-will-call tq nil
      #'(lambda (alist key obj)
	   (unless (assoc key alist)
	      (throw 'tinydb-q-new-obj 
		 (cons (cons key obj) alist))))
      key 
      obj))
;;;_   , tinydb-alist-push-replace
(defun tinydb-alist-push-replace (tq key obj)
   "Add a cell of (KEY . OBJ) onto an alist managed by TQ, replacing
any previous KEY cell."
   (tinydb-q-will-call tq nil
      #'(lambda (alist key obj)
	   (throw 'tinydb-q-new-obj 
	      (cons 
		 (cons key obj) 
		 (tinydb-alist--remove alist key))))
      key 
      obj))

;;;_   , tinydb-alist-assoc
(defun tinydb-alist-assoc (tq key)
   "Get cell corresponding to KEY from an alist managed by TQ."
   (tinydb-get tq
      #'(lambda (alist key)
	   (assoc key alist))
      key))
;;;_   , persist--alist-remove
(defsubst tinydb-alist--remove (alist key)
   ""
   (delete* key alist :key #'car))

;;;_   , tinydb-alist-update
(defun tinydb-alist-update (tq key update-f &optional now-p)
   "Replaces the matching object with an updated version
TQ must be an asynq.
KEY is a key.
UPDATE-F is a function to update the value.
 * Takes the old object (or nil if none)
 * Takes a flag, whether there was an old object found.
 * Takes the KEY.
 * Returns the new value for the cdr (not including the key)"

   (tinydb-q-will-call tq now-p
      #'(lambda (alist key update-f)
	   (let* 
	      ((cell
		  (assoc key alist))
		 ;;The new list won't have that cell, it will have
		 ;;another object with that key, so remove the
		 ;;original.
		 (new-list
		    (if cell
		       (tinydb-alist--remove alist key)
		       alist))
	      
		 ;;Get the new cell
		 (new-cell
		    (cons key
		       (if cell
			  (funcall update-f (cdr cell) t   key)
			  (funcall update-f nil        nil key)))))
	   
	      ;;Write the alist back
	      (throw 'tinydb-q-new-obj (cons new-cell new-list))))
      key
      update-f))


;;;_. Footers
;;;_ , Provides

(provide 'tinydb/asynq)

;;;_ * Local emacs vars.
;;;_  + Local variables:
;;;_  + mode: allout
;;;_  + End:

;;;_ , End
;;; tinydb/asynq.el ends here
