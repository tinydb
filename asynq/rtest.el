;;;_ tinydb/rtest.el --- Rtest tests for persist

;;;_. Headers
;;;_ , License
;; Copyright (C) 2010  Tom Breton (Tehom)

;; Author: Tom Breton (Tehom) <tehom@panix.com>
;; Keywords: lisp, maint, internal

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;;_ , Commentary:

;; 


;;;_ , Requires
(require 'tinydb/asynq)
;;;_. Body
;;;_  . Test helpers
(defun persist:th:make-usual-tq (initial)
   ""
   (tinydb-make-q
      ;;Create.
      #'identity 
      ;;Get
      #'identity 
      ;;Put
      #'(lambda (old-obj obj)
	   obj)
      ;;Type predicate that always passes
      #'list
      ;;Initial value
      initial
      ))
;;;_   , tinydb-alist-push
(rtest:deftest tinydb-alist-push
   (  "Operation: Push an element"
      (with-timeout (1.5)
	 (let
	    ((list '())
	       (tq
		  (persist:th:make-usual-tq '((b 144)))))
	    
	    (tinydb-alist-push tq 'a '(12))
	    (tinydb-q-do-pending tq)
	    (assert
	       (rtest:sets=
		  (tinydb-q->obj tq)
		  '((a 12) (b 144)))
	       t)
	    
	    t))))

;;;_   , tinydb-alist-assoc
(rtest:deftest tinydb-alist-assoc
   (  "Proves: Can read from it via `tinydb-alist-assoc'."
      (with-timeout (1.5)
	 (let*
	    ((list '((a 12)(b 144)))
	       (tq
		  (persist:th:make-usual-tq list))
	       (result 
		  (tinydb-alist-assoc tq 'a)))

	    (assert
	       (equal 
		  (tinydb-q->obj tq)
		  list)
	       t)
	    (assert
	       (equal 
		  result
		  '(a 12))
	       t)
	    
	    t))))
;;;_   , tinydb-alist--remove
(rtest:deftest tinydb-alist--remove

   (  "Gives the expected results"
      (progn
	 (assert
	    (equal
	       (tinydb-alist--remove (list '(1 a)'(2 b)) 1)
	       '((2 b)))
	    t)
	 (assert
	    (equal
	       (tinydb-alist--remove (list '(1 a)'(2 b)) 2)
	       '((1 a)))
	    t)
	 t)))

;;;_   , tinydb-alist-update

(rtest:deftest tinydb-alist-update
   (  "Proves: It updates an element."
      (with-timeout (1.5)
	 (let*
	    (  (list (list (list 'a 12)(list 'b 144)))
	       (tq
		  (persist:th:make-usual-tq list)))
	    (tinydb-alist-update tq 'a
	       #'(lambda (old old-p key)
		    (assert (equal old '(12)) t)
		    (assert (equal key 'a) t)
		    '(1728)))
	    (tinydb-q-do-pending tq)
	    (assert
	       (rtest:sets=
		  (tinydb-q->obj tq)
		  '((a 1728)(b 144)))
	       t)
	    
	    (assert
	       (equal 
		  (tinydb-alist-assoc tq 'a)
		  '(a 1728))
	       t)
	    t))))

;;;_   , tinydb-alist-pushnew
(rtest:deftest tinydb-alist-pushnew

   (  "Situation: Element is not already there.
Response: Add it."
      (with-timeout (1.5)
	 (let*
	    (  (list (list (list 'a 12)(list 'b 144)))
	       (tq
		  (persist:th:make-usual-tq list)))
	    (tinydb-alist-pushnew tq 'c '(1728))
	    (tinydb-q-do-pending tq)
	    (assert
	       (rtest:sets=
		  (tinydb-q->obj tq)
		  '((a 12)(b 144)(c 1728)))
	       t)
	    
	    (assert
	       (equal 
		  (tinydb-alist-assoc tq 'c)
		  '(c 1728))
	       t)
	    t)))
   
   (  "Situation: Element is already there.
Response: No change."
      (with-timeout (1.5)
	 (let*
	    (  (list (list (list 'a 12)(list 'b 144)))
	       (tq
		  (persist:th:make-usual-tq list)))
	    (tinydb-alist-pushnew tq 'a '(13))
	    (tinydb-q-do-pending tq)

	    (assert
	       (rtest:sets=
		  (tinydb-q->obj tq)
		  '((a 12)(b 144)))
	       t)
	    
	    (assert
	       (equal 
		  (tinydb-alist-assoc tq 'a)
		  '(a 12))
	       t)
	    t))))
;;;_   , tinydb-alist-push-replace
(rtest:deftest tinydb-alist-push-replace

   (  "Situation: Element is not already there.
Response: Add it."
      (with-timeout (1.5)
	 (let*
	    (  (list (list (list 'a 12)(list 'b 144)))
	       (tq
		  (persist:th:make-usual-tq list)))
	    (tinydb-alist-push-replace tq 'c '(1728))
	    (tinydb-q-do-pending tq)
	    (assert
	       (rtest:sets=
		  (tinydb-q->obj tq)
		  '((a 12)(b 144)(c 1728)))
	       t)
	    
	    (assert
	       (equal 
		  (tinydb-alist-assoc tq 'c)
		  '(c 1728))
	       t)
	    t)))
   
   (  "Situation: Element is already there.
Response: It replaces the old element."
      (with-timeout (1.5)
	 (let*
	    (  (list (list (list 'a 12)(list 'b 144)))
	       (tq
		  (persist:th:make-usual-tq list)))
	    (tinydb-alist-push-replace tq 'a '(1728))
	    (tinydb-q-do-pending tq)

	    (assert
	       (rtest:sets=
		  (tinydb-q->obj tq)
		  '((a 1728)(b 144)))
	       t)
	    
	    (assert
	       (equal 
		  (tinydb-alist-assoc tq 'a)
		  '(a 1728))
	       t)
	    t))))

;;;_  . Reentrancy tests

;;These tests are theoretically sensitive to processor speed, but the
;;specified durations are extremely generous so there shouldn't be a
;;problem.
(rtest:deftest tinydb/reentrancy
   
   (  "Proves: Timer events can run during sleep-for."
      (let
	 ((recorded '()))
	 (run-with-timer 0.1 nil
	    #'(lambda ()
		 (push (list 'timed (current-time)) recorded)))
	 (sleep-for 1.0)
	 (push (list 'delayed (current-time)) recorded)
	 (assert
	    (assq 'timed recorded)
	    t)
	 (assert
	    (assq 'delayed recorded)
	    t)	 
	 (let* 
	    (  (timed-ran-at   (second (assq 'timed   recorded)))
	       (delayed-ran-at (second (assq 'delayed recorded)))
	       (time-diff (time-subtract delayed-ran-at timed-ran-at)))
	    
	    (assert
	       (time-less-p time-diff (seconds-to-time 0.99))
	       t)
	    (assert
	       (time-less-p (seconds-to-time 0.80) time-diff)
	       t)	    
	    t)))
   
   (  "Proves: Validates the testing mechanism with sit-for vs a
      single timer."
      (with-timeout (1.5)
	 (let
	    ((list '()))

	    (run-with-timer 0.2 nil
	       #'(lambda ()
		    (push "Start 2" list)
		    (sit-for 0.2)
		    (push "End 2" list)))

	    (push "Start test" list)
	    (sit-for 0.2)
	    (push "End test" list)
	    (assert
	       (equal
		  (reverse list)
		  '("Start test" "Start 2" "End 2" "End test"))
	       t)
	    t)))

   (  "Proves: Validates the testing mechanism with multiple run-with-timers.
      (which somehow nests their sit-for calls)"
      (with-timeout (1.5)
	 (let
	    ((list '()))
	    (run-with-timer 0.1 nil
	       #'(lambda ()
		    (push "Start 1" list)
		    (sit-for 0.1)
		    (sit-for 0.1)
		    (push "End 1" list)))
	    (run-with-timer 0.2 nil
	       #'(lambda ()
		    (push "Start 2" list)
		    (sit-for 0.1)
		    (sit-for 0.1)
		    (push "End 2" list)))

	    (push "Start test" list)
	    (sit-for 0.6)
	    (push "End test" list)
	    (assert
	       (equal
		  (reverse list)
		  '("Start test" "Start 1" "Start 2" 
		      "End 2" "End 1" "End test"))
	       t)
	    t)))
   

   (  "Proves: Can see the internal object (at all)."
      (with-timeout (1.5)
	 (let*
	    ((list '())
	       (tq
		  (persist:th:make-usual-tq '(12)))
	       (result nil))

	    (tinydb-q-will-call
	       tq t
	       #'(lambda (obj)
		    (setq result obj)))
	    (assert
	       (equal 
		  (tinydb-q->obj tq)
		  '(12))
	       t)
	    (assert
	       (equal 
		  result
		  '(12))
	       t)
	    t)))

   (  "Proves: Calling `tinydb-q-will-call' recursively in handler
      gives an error.
FIX ME:  This logic has changed.  Now it's only reads that require
errors to be raised.
"
      (with-timeout (1.5)
	 (let
	    ((list '())
	       (tq
		  (persist:th:make-usual-tq '(12))))
	    
	    (push "Start test" list)
	    (tinydb-q-will-call
	       tq nil
	       #'(lambda (obj)
		    (assert
		       (emth:gives-error
			  (tinydb-q-will-call tq t #'identity))
		       t)))

	    (push "End test" list)
	    (assert
	       (equal
		  (reverse list)
		  '("Start test" "End test"))
	       t)
	    (assert
	       (equal 
		  (tinydb-q->obj tq)
		  '(12))
	       t)
	    t)))

   '  ;;No longer guaranteed
   (  "Proves: Can write asynchronously to it."
      (with-timeout (1.5)
	 (let
	    ((list '())
	       (tq
		  (persist:th:make-usual-tq '(12))))
	    (push "Start test" list)
	    (run-with-timer 0.1 nil
	       #'(lambda (arg)
		    (push "Start 1" list)
		    (tinydb-q-will-call
		       tq nil
		       #'(lambda (obj x)
			    (sleep-for 0.2)
			    (push "End 1" list)
			    (throw 'tinydb-q-new-obj
			       (cons x obj)))
		       arg))
	       144)
	    
	    (run-with-timer 0.2 nil
	       #'(lambda (arg)
		    (push "Start 2" list)
		    (tinydb-q-will-call
		       tq nil
		       #'(lambda (obj x)
			    (sleep-for 0.2)
			    (throw 'tinydb-q-new-obj 
			       (cons x obj)))
		       arg))
	        1728)
	    
	    (sit-for 0.6)
	    (tinydb-q-do-pending tq)
	    (push "End test" list)
	    (assert
	       (equal
		  (reverse list)
		  '("Start test" "Start 1" "Start 2" 
		      "End 1" "End test"))
	       t)
	    (assert
	       (rtest:sets=
		  (tinydb-q->obj tq)
		  '(1728 144 12))
	       t)
	    t)))

   
   )


;;;_. Footers
;;;_ , Provides

(provide 'tinydb/rtest)

;;;_ * Local emacs vars.
;;;_  + Local variables:
;;;_  + mode: allout
;;;_  + End:

;;;_ , End
;;; tinydb/rtest.el ends here
