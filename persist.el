;;;_ tinydb/persist.el --- Create persisting data

;;;_. Headers
;;;_ , License
;; Copyright (C) 2007,2010  Tom Breton (Tehom)

;; Author: Tom Breton (Tehom) <tehom@panix.com>
;; Keywords: 

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;;_ , Commentary:

;; This was originally part of my `password' library.  I separated it
;; so that it could be re-used.

;;Drawbacks: It only makes one object persist per buffer.  This could
;;be remedied with a major rewrite.  Even so, it would probably be
;;less safe.

;;;_ , Requires
(require 'pp)
(require 'tinydb/asynq)


;;;_. Code:

;;;_ , Internal:

;;;_  . tinydb-persist--write-obj
(defun tinydb-persist--write-obj (obj &optional force-save)
   ""

   (let
      ((old-str (buffer-string)))
      (erase-buffer)
      (insert (pp-to-string obj))
      ;;If it can't be read back, it's an error
      (condition-case err
	 (progn
	    (goto-char (point-min))
	    (read (current-buffer)))
	 (error
	    (erase-buffer)
	    (insert old-str)
	    (apply #'signal (car err) (cdr err)))))
   
   (when force-save
      ;;Save, forcing backups to exist.
      (let
	 ((file-precious-flag t))
	 (save-buffer 64))))


;;;_ , File-buffer instantiation of asynq

;;;_  . tinydb-persist-make-q
(defun tinydb-persist-make-q 
   (filename initial-obj &optional eager-save type-pred)
   "Make a file-based persisting queue"
   (tinydb-make-q
      ;;This is the setup for file-based asynq
      #'(lambda (filename initial-object eager-save)
	   (with-current-buffer
	      (find-file-noselect filename eager-save)
	      (declare (special persist-eager-save))
	      (set 
		 (make-local-variable
		    'persist-eager-save) 
		 eager-save)
	      (when
		 (= (buffer-size) 0)
		 (tinydb-persist--write-obj initial-object eager-save))
	      (current-buffer)))
		   
      ;;get
      #'(lambda (buffer)
	   (with-current-buffer buffer
	      (condition-case err
		 (progn
		    (goto-char (point-min))
		    (read (current-buffer)))
		 (error 
		    (error
		       "In persist buffer %s, contents could not be read"
		       (current-buffer))
		    '()))))
		   
      ;;put
      #'(lambda (buffer obj)
	   (with-current-buffer buffer
	      (declare (special persist-eager-save))
	      (tinydb-persist--write-obj obj persist-eager-save)
	      buffer))
      type-pred
      filename initial-obj eager-save))
;;;_  . Associating files to objects
;;;_  . tinydb:filename-alist
;;$$IMPROVE ME Detect and restart deleted buffers.
(defvar tinydb:filename-alist 
   '()
   "Alist from absolute filenames to tinydb objects" )

;;;_  . tinydb:filename->tinydb
(defun tinydb:filename->tinydb (filename) 
   "Return a tinydb object visiting FILENAME."
   (or
      (let*
	 ((cell (assoc filename tinydb:filename-alist))
	    (buf (second cell)))
	 (when (buffer-live-p buf)
	    buf))
      (let 
	 ((filetq
	     (tinydb-persist-make-q filename '() nil #'listp)))
	 (push (list filename filetq) tinydb:filename-alist)
	 filetq)))


;;;_: Footers

;;;_ * Local emacs vars.
;;;_  + Local variables:
;;;_  + mode: allout
;;;_  + End:

(provide 'tinydb/persist)
;;; tinydb/persist.el ends here
