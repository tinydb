;;;_ tinydb/persist/rtest.el --- Rtest tests for persist

;;;_. Headers
;;;_ , License
;; Copyright (C) 2010  Tom Breton (Tehom)

;; Author: Tom Breton (Tehom) <tehom@panix.com>
;; Keywords: lisp, maint, internal

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;;_ , Commentary:

;; 


;;;_ , Requires

(require 'tinydb/persist)
(require 'emtest/testhelp/tagnames)
(require 'emtest/testhelp/mocks/filebuf)
(require 'emtest/testhelp/testpoint)
(require 'timer)

;;;_. Body
;;;_  . Test data

(defconst persist:th:examples-dir
   (emtb:expand-filename-by-load-file "examples/") 
   "Directory where examples are" )

;;The roles here are out of sync
(defconst tinydb:thd:examples
   (emtg:define+ ;;xmp:b5d33625-bd1d-49d3-9c56-148b699eba99
      ((project emtest)(library persist))
      (group ((type filename))
	 (item ((role master))
	    (expand-file-name "1" persist:th:examples-dir))
	 (item ((role slave))
	    (expand-file-name "2" persist:th:examples-dir)))
   
      (group ((creation-time before))
	 (item ((type data)) '(1 10 500)))
   
      (group ((creation-time now))
	 (item ((type data)) '(56 25 17)))))


;;;_   , tehom-persist-buffer-as-const-list
;;$$OBSOLESCENT.
'
(rtest:deftest tehom-persist-buffer-as-const-list

   (  "Situation: The file exists; we know what data it contains.
Operation: Get that data
Result: The data is what we expect."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (tehom-persist-buffer-as-const-list
	    (emtg (type filename)(role master)) 
	    a
	    (equal a
	       (emtg (type data)(creation-time before))))))

   (  "Situation: The file doesn't exist.
Result: It returns the initial value - which is ()
This does not attempt to create the file or save the buffer."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f (:absent t) filename
	    (let
	       (
		  ;;Tiny touch of psychic here: we know that the initial
		  ;;list value is ().
		  (initial-value ()))
	       (tehom-persist-buffer-as-const-list
		  filename 
		  a
		  (assert
		     (equal a initial-value)
		     t))
	       t))))
   
   )
;;;_   , tinydb/buffer Same tests on the alist wrt files

(rtest:deftest tinydb/buffer

   (  "Situation: The file exists; we know what data it contains.
Operation: Get that data
Result: The data is what we expect."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (let
	    ((filetq
		(tinydb-persist-make-q
		   ;;Filename
		   (emtg (type filename)(role master))
		   ;;Initial object
		   '()
		   t)))
	    
	    ;;Test that we can read it.
	    (assert
	       (equal 
		  (tinydb-get-obj filetq)
		  (emtg (type data)(creation-time before))))
	    t)))
   

   (  "Situation: The file doesn't exist.
Result: It returns the initial value.
This does not attempt to create the file or save the buffer."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f (:absent t) filename
	    (let*
	       (
		  (initial-value '(a b c))
		  (filetq
		     (tinydb-persist-make-q
			;;Filename
			filename
			;;Initial object
			initial-value
			nil)))
	       (assert
		  (equal 
		     (tinydb-get-obj filetq)
		     initial-value))
	       ;;The file still doesn't exist, because it was not
	       ;;eager-save and no operation we did should have forced
	       ;;a save.
	       (assert
		  (not (file-exists-p filename))
		  t)
	       t))))
   
   )

;;;_   , tehom-persist-buffer-as-mutable-obj
;;$$OBSOLESCENT.
(put 'tehom-persist-buffer-as-mutable-obj 'rtest:test-thru
   'tehom-persist-buffer-as-mutable-list)
;;;_   , tehom-persist-buffer-as-mutable-list
;;$$OBSOLESCENT.
'
(rtest:deftest tehom-persist-buffer-as-mutable-list

   (  "Proves: Form return value is the same as body return value."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (let*
	    ((master-file 
		(emtg (role master)(type filename)))
	       (ret-val
		  (tehom-persist-buffer-as-mutable-list
		     master-file a
		     135)))
	    
	    (assert (equal ret-val 135) t)
	    t)))
   
   (  "Situation: Read a persisting object.  
Operation: Set a different value.  Read it as if fresh.
Afterwards: It retains the new value."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f
	    (:file (emtg (role master)(type filename))) 
	    filename
	    (tehom-persist-buffer-as-mutable-list
	       filename
	       a
	       ;;Validate: We got the expected original value.
	       (equal a
		  (emtg (creation-time before)(type data)))
	       ;;Set it to the now value.
	       (setq a
		  (emtg (creation-time now)(type data))))
	    ;;Read it as if fresh
	    (tehom-persist-buffer-as-const-list
	       filename
	       a
	       (equal a
		  (emtg (type data)(creation-time now)))))))
      

   (  "Situation: The file doesn't exist.
Result: It returns the initial value - which is ()
File now exists and contains the initial value."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f (:absent t) filename
	    ;;Tiny touch of psychic here: we know that the initial
	    ;;list value is ().
	    (let
	       (
		  (initial-value ()))
	       (tehom-persist-buffer-as-mutable-list
		  filename 
		  a
		  (assert 
		     (equal a initial-value)
		     t))
	       (assert
		  (file-exists-p filename)
		  t)
	       (let*
		  ((text (emtb:file-contents-absname filename))
		     (obj (if text (read text))))
		  (assert
		     (equal obj
			())
		     t))
	       t))))
   
   )
;;;_   , tinydb/buffer/alist/mutate
;;New version
(rtest:deftest tinydb/buffer/alist/mutate

   (  "Situation: Read a persisting object.  
Operation: Set a different value.  Read it as if fresh.
Afterwards: It retains the new value."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f
	    (:file (emtg (role master)(type filename))) 
	    filename
	    (let*
	       (
		  (filetq
		     (tinydb-persist-make-q
			;;Filename
			filename
			;;Initial object
			'()
			t)))
	       ;;Validate: We got the expected original value.
	       (assert
		  (equal
		     (tinydb-get-obj filetq)
		     (emtg (creation-time before)(type data)))
		  t)
	       
	       ;;Set it to the new value.
	       (tinydb-set-obj 
		  filetq
		  (emtg (creation-time now)(type data)))
	       
	       ;;The new value is immediately available.
	       (let
		  ((value (tinydb-get-obj filetq)))
		  (assert
		     (equal
			value
			(emtg (creation-time now)(type data)))
		     t))
	       t))))


   (  "Situation: The file doesn't exist.
Result: It returns the initial value - which is ()
File now exists and contains the initial value."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f (:absent t) filename
	    (let*
	       (
		  (initial-value '(12)) ;;Distinct from nil
		  (filetq
		     (tinydb-persist-make-q
			;;Filename
			filename
			;;Initial object
			initial-value
			t)))
	       (assert
		  (equal 
		     (tinydb-get-obj filetq)
		     initial-value))
	       ;;Eager save causes the file to exist.
	       (assert
		  (file-exists-p filename)
		  t)
	       ;;The file's contents are as expected. 
	       (let*
		  ((text (emtb:file-contents-absname filename))
		     (obj (if text (read text))))
		  (assert
		     (equal obj initial-value)
		     t))
	       t))))
   
   )

;;;_   , tehom-update-persist-buffer
;;OBSOLESCENT
'
(rtest:deftest tehom-update-persist-buffer

   (  "Operation: Write a different value.
Afterwards: Read it as if fresh.  It retains the new value."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f
	    (:file (emtg (role master)(type filename))) 
	    filename
	    ;;Save the new value.
	    (tehom-update-persist-buffer 
	       filename
	       (emtg (creation-time now)(type data))
	       t)
	    ;;Read it as if fresh
	    (tehom-persist-buffer-as-const-list
	       filename
	       a
	       (equal a
		  (emtg (type data)(creation-time now)))))))
   
   (  "Operation: Try to write a different value that fails the
type-predicate.
Result: Signal an error.
Afterwards: Read it as if fresh.  It still has the old value."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f
	    (:file (emtg (role master)(type filename))) filename
	    ;;Try to save a value that fails the type predicate.
	    ;;This must signal error.
	    (assert
	       (emth:gives-error
		  (tehom-update-persist-buffer 
		     filename
		     'not-a-list
		     t
		     #'listp)))
	    ;;Read it as if fresh
	    (tehom-persist-buffer-as-const-list
	       filename
	       a
	       ;;It still has the old value.
	       (equal a
		  (emtg (type data)(creation-time before)))))))
   
   (  "Operation: Try to write a value that can't be read - In fact, a
buffer object.
Result: Signal an error.
Afterwards: Read it as if fresh.  It still has the old value."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f
	    (:file (emtg (role master)(type filename))) filename
	    ;;Try to save a value that fails the type predicate.
	    ;;This must signal error.
	    (assert
	       (emth:gives-error
		  (tehom-update-persist-buffer 
		     filename
		     (current-buffer)
		     t)))
	    ;;Read it as if fresh
	    (tehom-persist-buffer-as-const-list
	       filename
	       a
	       ;;It still has the old value.
	       (equal a
		  (emtg (type data)(creation-time before)))))))

   )

;;;_   , tinydb/buffer/update
;;New version
(rtest:deftest tinydb/buffer/update
   (  "Operation: Try to write a different value that fails the
type-predicate.
Result: Signal an error.
Afterwards: Read it as if fresh.  It still has the old value."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f
	    (:file (emtg (role master)(type filename))) filename

	    (let* 
	       ((filetq
		   (tinydb-persist-make-q
		      ;;Filename
		      filename
		      ;;Initial object (Unused here)
		      '()
		      t
		      #'listp)))
	       ;;Try to save a value that fails the type predicate.
	       (tinydb-set-obj filetq 'not-a-list)
	       ;;Object still has the old value.
	       (assert
		  (equal 
		     (tinydb-get-obj filetq)
		     (emtg (type data)(creation-time before))))
	       t))))
   
   ;;The last one remaining to convert
   (  "Operation: Try to write a value that can't be read - In fact, a
buffer object.
Result: Signal an error.
Afterwards: Read it as if fresh.  It still has the old value."
      (emtg:with tinydb:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f
	    (:file (emtg (role master)(type filename))) filename

	    (let* 
	       ((filetq
		   (tinydb-persist-make-q
		      ;;Filename
		      filename
		      ;;Initial object (Unused here)
		      '()
		      t
		      #'listp)))
	       ;;Try to save a value that can't be read.
	       (tinydb-set-obj filetq (current-buffer))
	       ;;Object still has the old value.
	       (assert
		  (equal 
		     (tinydb-get-obj filetq)
		     (emtg (type data)(creation-time before))))
	       t))))

   )



;;;_. Footers
;;;_ , Provides

(provide 'tinydb/persist/rtest)

;;;_ * Local emacs vars.
;;;_  + Local variables:
;;;_  + mode: allout
;;;_  + End:

;;;_ , End
;;; tinydb/persist/rtest.el ends here
