;;;_ tinydb/persist/tests.el ---  tests for persist

;;;_. Headers
;;;_ , License
;; Copyright (C) 2010  Tom Breton (Tehom)

;; Author: Tom Breton (Tehom) <tehom@panix.com>
;; Keywords: lisp, maint, internal

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;;_ , Commentary:

;; 


;;;_ , Requires

(require 'tinydb/persist)
(require 'emtest/testhelp/tagnames)
(require 'emtest/testhelp/mocks/filebuf)
(require 'emtest/testhelp/testpoint)
(require 'timer)
(defconst persist:th:examples-dir
   (emtb:expand-filename-by-load-file "examples/") 
   "Directory where examples are" )

;;;_. Body
;;;_  . Test data

;;The roles here are out of sync
(defconst tinydb/persist:thd:examples
   (emtg:define+ ;;xmp:b5d33625-bd1d-49d3-9c56-148b699eba99
      ((project emtest)(library persist))
      (group ((type filename))
	 (item ((role master))
	    (expand-file-name "1" persist:th:examples-dir))
	 (item ((role slave))
	    (expand-file-name "2" persist:th:examples-dir)))
   
      (group ((creation-time before))
	 (item ((type data)) '(1 10 500)))
   
      (group ((creation-time now))
	 (item ((type data)) '(56 25 17)))))


;;;_   , tinydb/buffer Same tests on the alist wrt files

(emt:deftest-3 tinydb/buffer
   (()
      (emtg:with tinydb/persist:thd:examples
	 ((project emtest)(library persist))
	 (let
	    ((filetq
		(tinydb-persist-make-q
		   ;;Filename
		   (emtg (type filename)(role master))
		   ;;Initial object
		   '()
		   t)))
	    (emt:doc 
	       "Situation: The file exists; we know what data it contains.")

	    ;;Test that we can read it.
	    (emt:doc "Operation: Get that data.")
	    (emt:doc "Result: The data is what we expect.")
	    (assert
	       (equal 
		  (tinydb-get-obj filetq)
		  (emtg (type data)(creation-time before)))))))
   (nil
      (emtg:with tinydb/persist:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f (:absent t) filename
	    (let*
	       (
		  (initial-value ())
		  (filetq
		     (tinydb-persist-make-q
			;;Filename
			filename
			;;Initial object
			initial-value
			nil)))
	       (emt:doc "Situation: The file doesn't already exist.")
	       (emt:doc "Param: The eager-save flag is nil.")
	       (emt:doc "Operation: read from the filetq.")
	       (emt:doc "Result: It returns the initial value.")
		  
	       (assert
		  (equal 
		     (tinydb-get-obj filetq)
		     initial-value))

	       ;;The file still doesn't exist, because it was not
	       ;;eager-save and no operation we did should have
	       ;;forced a save.
	       (emt:doc "Check: Whether the file exists now.")
	       (emt:doc "Result: It returns the initial value.")
	       (assert
		  (not (file-exists-p filename))
		  t)
	       t))))
   )

;;;_   , tinydb/buffer/alist/mutate
(emt:deftest-3  tinydb/buffer/alist/mutate

   (  ()
      (emtg:with tinydb/persist:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f
	    (:file (emtg (role master)(type filename))) 
	    filename
	    (emt:doc "Situation: Read a persisting object.")
	    (emt:doc "Operation: Set a different value.  Read it as if fresh.")
	    (emt:doc "Afterwards: It retains the new value.")
	    (let*
	       (
		  (filetq
		     (tinydb-persist-make-q
			;;Filename
			filename
			;;Initial object
			'()
			t)))
	       ;;Validate: We got the expected original value.
	       (assert
		  (equal
		     (tinydb-get-obj filetq)
		     (emtg (creation-time before)(type data)))
		  t)
	       
	       ;;Set it to the new value.
	       (tinydb-set-obj 
		  filetq
		  (emtg (creation-time now)(type data)))
	       
	       ;;The new value is immediately available.
	       (let
		  ((value (tinydb-get-obj filetq)))
		  (assert
		     (equal
			value
			(emtg (creation-time now)(type data)))
		     t))
	       t))))


   (  ()
      (emtg:with tinydb/persist:thd:examples
	 ((project emtest)(library persist))
	 (emtb:with-file-f (:absent t) filename
	    (let*
	       (
		  (initial-value '(12)) ;;Distinct from nil
		  (filetq
		     (tinydb-persist-make-q
			;;Filename
			filename
			;;Initial object
			initial-value
			t)))
	       (emt:doc "Situation: The file doesn't yet exist.")
	       (emt:doc "Situation: The eager-save flag is set.")

	       ;;Eager save causes the file to exist.
	       (emt:doc "Afterwards: File now exists.")
	       (assert
		  (file-exists-p filename)
		  t)

	       (emt:doc "Afterwards: file's contents are the initial value.")
	       (let*
		  ((text (emtb:file-contents-absname filename))
		     (obj (if text (read text))))
		  (assert
		     (equal obj initial-value)
		     t))
	       (emt:doc "Operation: Read it.")
	       (emt:doc "Result: It returns the initial value.")
	       (assert
		  (equal 
		     (tinydb-get-obj filetq)
		     initial-value))
	       ))))
   
   )

;;;_   , tinydb/buffer/update
(emt:deftest-3 tinydb/buffer/update
   (nil
      (progn
	 (emt:doc "Operation: Try to write a different value that fails the
type-predicate.")
	 (emt:doc "Result: Signal an error.")
	 (emt:doc "Afterwards: Read it as if fresh.  It still has the old value.")
	 (emtg:with tinydb/persist:thd:examples
	    ((project emtest)
	       (library persist))
	    (emtb:with-file-f
	       (:file
		  (emtg
		     (role master)
		     (type filename)))
	       filename
	       (let*
		  ((filetq
		      (tinydb-persist-make-q filename 'nil t #'listp)))
		  (emt:doc "Operation: Try to save a value that fails the type predicate.")
		  (tinydb-set-obj filetq 'not-a-list)
		  (emt:doc "Result: Object still has the old value.")
		  (assert
		     (equal
			(tinydb-get-obj filetq)
			(emtg
			   (type data)
			   (creation-time before))))
		  t)))))
   (nil
      (progn
	 (emt:doc "Operation: Try to write a value that can't be read - In fact, a
buffer object.")
	 (emt:doc "Result: Signal an error.
")
	 (emt:doc "Afterwards: Read it as if fresh.  It still has the old value.")
	 (emtg:with tinydb/persist:thd:examples
	    ((project emtest)
	       (library persist))
	    (emtb:with-file-f
	       (:file
		  (emtg
		     (role master)
		     (type filename)))
	       filename
	       (let*
		  ((filetq
		      (tinydb-persist-make-q filename 'nil t #'listp)))
		  (emt:doc "Operation: Try to save a value that can't be read.")
		  (tinydb-set-obj filetq
		     (current-buffer))
		  (emt:doc "Result: Object still has the old value.")
		  (assert
		     (equal
			(tinydb-get-obj filetq)
			(emtg
			   (type data)
			   (creation-time before))))
		  t))))))

;;;_   , Test helpers
(defun persist:th:make-usual-tq (initial)
   ""
   (tinydb-make-q
      ;;Create.
      #'identity 
      ;;Get
      #'identity 
      ;;Put
      #'(lambda (old-obj obj)
	   obj)
      ;;Type predicate that always passes
      #'list
      ;;Initial value
      initial
      ))
;;;_   , tinydb-alist-push
(emt:deftest-3 tinydb-alist-push
   (nil
      (progn
	 (emt:doc "Proves: Can write to it (via `tinydb-alist-push').")
	 (with-timeout
	    (1.5)
	    (let
	       ((list 'nil)
		  (tq
		     (persist:th:make-usual-tq
			'((b . 144)))))
	       (tinydb-alist-push tq 'a 12)
	       (tinydb-q-do-pending tq)
	       (assert
		  (equal
		     (tinydb-q->obj tq)
		     '((a . 12) (b . 144)))
		  t)
	       t)))))


;;;_   , tinydb-alist-assoc
(emt:deftest-3 tinydb-alist-assoc
   (nil
      (progn
	 (emt:doc "Proves: Can read from it via `tinydb-alist-assoc'.")
	 (with-timeout
	    (1.5)
	    (let*
	       ((list 'nil)
		  (tq
		     (persist:th:make-usual-tq
			'((a 144))))
		  (result
		     (tinydb-alist-assoc tq 'a)))
	       (assert
		  (equal
		     (tinydb-q->obj tq)
		     '((a 144)))
		  t)
	       (assert
		  (equal result
		     '(a 144))
		  t)
	       t)))))


;;;_   , Reentrancy tests

;;These tests are theoretically sensitive to processor speed, but the
;;specified durations are extremely generous so there shouldn't be a
;;problem.
(emt:deftest-3 tinydb/reentrancy
   (nil
      (progn
	 (emt:doc "Proves: Timer events can run during sleep-for.")
	 (let
	    ((recorded 'nil))
	    (run-with-timer 0.1 nil
	       #'(lambda nil
		    (push
		       (list 'timed
			  (current-time))
		       recorded)))
	    (sleep-for 1.0)
	    (push
	       (list 'delayed
		  (current-time))
	       recorded)
	    (assert
	       (assq 'timed recorded)
	       t)
	    (assert
	       (assq 'delayed recorded)
	       t)
	    (let*
	       ((timed-ran-at
		   (second
		      (assq 'timed recorded)))
		  (delayed-ran-at
		     (second
			(assq 'delayed recorded)))
		  (time-diff
		     (time-subtract delayed-ran-at timed-ran-at)))
	       (assert
		  (time-less-p time-diff
		     (seconds-to-time 0.99))
		  t)
	       (assert
		  (time-less-p
		     (seconds-to-time 0.8)
		     time-diff)
		  t)
	       t))))
   (nil
      (progn
	 (emt:doc "Proves: Validates the testing mechanism with sit-for vs a
      single timer.")
	 (with-timeout
	    (1.5)
	    (let
	       ((list 'nil))
	       (run-with-timer 0.2 nil
		  #'(lambda nil
		       (push "Start 2" list)
		       (sit-for 0.2)
		       (push "End 2" list)))
	       (push "Start test" list)
	       (sit-for 0.2)
	       (push "End test" list)
	       (assert
		  (equal
		     (reverse list)
		     '("Start test" "Start 2" "End 2" "End test"))
		  t)
	       t))))
   (nil
      (progn
	 (emt:doc "Proves: Validates the testing mechanism with multiple run-with-timers.
      (which somehow nests their sit-for calls)")
	 (with-timeout
	    (1.5)
	    (let
	       ((list 'nil))
	       (run-with-timer 0.1 nil
		  #'(lambda nil
		       (push "Start 1" list)
		       (sit-for 0.1)
		       (sit-for 0.1)
		       (push "End 1" list)))
	       (run-with-timer 0.2 nil
		  #'(lambda nil
		       (push "Start 2" list)
		       (sit-for 0.1)
		       (sit-for 0.1)
		       (push "End 2" list)))
	       (push "Start test" list)
	       (sit-for 0.6)
	       (push "End test" list)
	       (assert
		  (equal
		     (reverse list)
		     '("Start test" "Start 1" "Start 2" "End 2" "End 1" "End test"))
		  t)
	       t))))
   (nil
      (progn
	 (emt:doc "Proves: Can see the internal object (at all).")
	 (with-timeout
	    (1.5)
	    (let*
	       ((list 'nil)
		  (tq
		     (persist:th:make-usual-tq
			'(144)))
		  (result nil))
	       (tinydb-q-will-call tq t
		  #'(lambda
		       (obj)
		       (setq result obj)))
	       (assert
		  (equal
		     (tinydb-q->obj tq)
		     '(144))
		  t)
	       (assert
		  (equal result
		     '(144))
		  t)
	       t))))
   (nil
      (progn
	 (emt:doc "Proves: Calling `tinydb-q-will-call' recursively in handler
      gives an error.
FIX ME:  This logic has changed.  Now it's only reads that require
errors to be raised.
")
	 (with-timeout
	    (1.5)
	    (let
	       ((list 'nil)
		  (tq
		     (persist:th:make-usual-tq
			'(144))))
	       (push "Start test" list)
	       (tinydb-q-will-call tq nil
		  #'(lambda
		       (obj)
		       (assert
			  (emth:gives-error
			     (tinydb-q-will-call tq t #'identity))
			  t)))
	       (push "End test" list)
	       (assert
		  (equal
		     (reverse list)
		     '("Start test" "End test"))
		  t)
	       (assert
		  (equal
		     (tinydb-q->obj tq)
		     '(144))
		  t)
	       t))))
   '
   (nil
      (progn
	 (emt:doc "Proves: Can write asynchronously to it.")
	 (with-timeout
	    (1.5)
	    (let
	       ((list 'nil)
		  (tq
		     (persist:th:make-usual-tq
			'(144))))
	       (push "Start test" list)
	       (run-with-timer 0.1 nil
		  #'(lambda
		       (arg)
		       (push "Start 1" list)
		       (tinydb-q-will-call tq nil
			  #'(lambda
			       (obj x)
			       (sleep-for 0.2)
			       (push "End 1" list)
			       (throw 'tinydb-q-new-obj
				  (cons x obj)))
			  arg))
		  12)
	       (run-with-timer 0.2 nil
		  #'(lambda
		       (arg)
		       (push "Start 2" list)
		       (tinydb-q-will-call tq nil
			  #'(lambda
			       (obj x)
			       (sleep-for 0.2)
			       (throw 'tinydb-q-new-obj
				  (cons x obj)))
			  arg))
		  1728)
	       (sit-for 0.6)
	       (tinydb-q-do-pending tq)
	       (push "End test" list)
	       (assert
		  (equal
		     (reverse list)
		     '("Start test" "Start 1" "Start 2" "End 1" "End test"))
		  t)
	       (assert
		  (rtest:sets=
		     (tinydb-q->obj tq)
		     '(1728 12 144))
		  t)
	       t)))))



;;;_. Footers
;;;_ , Provides

(provide 'tinydb/persist/tests)

;;;_ * Local emacs vars.
;;;_  + Local variables:
;;;_  + mode: allout
;;;_  + End:

;;;_ , End
;;; tinydb/persist/tests.el ends here
